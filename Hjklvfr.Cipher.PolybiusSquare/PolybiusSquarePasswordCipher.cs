﻿using System;
using System.Linq;
using Hjklvfr.Cipher.Abstractions;
using Hjklvfr.Cipher.Helpers;

namespace Hjklvfr.Cipher.PolybiusSquare
{
    public class PolybiusSquarePasswordCipher : IPasswordCipher
    {
        private char[,] _square;
        private readonly string _alphabet;
        private readonly string _alphabetPayload;

        public PolybiusSquarePasswordCipher(string alphabet = null, string alphabetPayload = null)
        {
            _alphabet = alphabet ?? "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
            _alphabetPayload = alphabetPayload ?? "0123456789!@#$%^&*)_+-=<>?,.";
        }

        private void CreateSquare(string password)
        {
            var newAlphabet =
                password.Aggregate(_alphabet, (current, t) =>
                    current.Replace(t.ToString(), ""));

            newAlphabet = password +
                          newAlphabet +
                          _alphabetPayload; //in order to avoid empty cells

            var sizeOfSquareSide = (int) Math.Ceiling(Math.Sqrt(_alphabet.Length));

            _square = new char[sizeOfSquareSide, sizeOfSquareSide];
            var index = 0;
            for (var i = 0; i < sizeOfSquareSide; i++)
            {
                for (var j = 0; j < sizeOfSquareSide; j++)
                {
                    if (index >= newAlphabet.Length) continue;
                    _square[i, j] = newAlphabet[index];
                    index++;
                }
            }
        }

        public string Encrypt(string text, string password)
        {
            CreateSquare(password);
            var outputText = "";
            foreach (var t in text)
            {
                if (!_square.FindSymbol(t, out var columnIndex, out var rowIndex)) continue;
                var newRowIndex = rowIndex == _square.GetUpperBound(1)
                    ? 0
                    : rowIndex + 1;
                outputText += _square[newRowIndex, columnIndex].ToString();
            }
            
            return outputText;
        }

        public string Decrypt(string text, string password)
        {
            CreateSquare(password);
            var outputText = "";
            var m = text.Length;
            for (var i = 0; i < m; i++)
            {
                if (!_square.FindSymbol(text[i], out var columnIndex, out var rowIndex)) continue;
                var newRowIndex = rowIndex == 0
                    ? _square.GetUpperBound(1)
                    : rowIndex - 1;
                outputText += _square[newRowIndex, columnIndex].ToString();
            }
            
            return outputText;
        }
    }
}