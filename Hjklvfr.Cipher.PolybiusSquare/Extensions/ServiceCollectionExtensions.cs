using Hjklvfr.Cipher.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace Hjklvfr.Cipher.PolybiusSquare.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddPolybiusSquareCipher(this IServiceCollection services)
        {
            services.AddScoped<IPasswordCipher, PolybiusSquarePasswordCipher>();
            return services;
        }
    }
}