﻿namespace CardanGrille

open System

module CardanGrille =
    let calculateBasement (fullCount: float) =
        Convert.ToInt32(Math.Round(Math.Sqrt(fullCount), MidpointRounding.ToPositiveInfinity))

    let calculateFinalCount basement = Math.Pow(basement, 2.0)

    let generateRandomString basement =
        let random = Random()
        let idx = random.Next(1, basement)
        String(Array.init basement (fun i -> if i = idx then '1' else '0'))

    let generateMask inputCount =
        seq { for i in 1 .. inputCount -> generateRandomString inputCount }
        |> Seq.fold
            (fun (sb: Text.StringBuilder) s -> sb.Append($"{s}{Environment.NewLine}"))
            (Text.StringBuilder())
        |> fun x -> x.ToString()

    let chars = "ABCDEFGHIJKLMNOPQRSTUVWUXYZ"

    let randomChar () =
        let random = Random()
        chars.[random.Next(chars.Length)]

    let randomizeString (maskStr: string) =
        String(
            Array.init
                maskStr.Length
                (fun i ->
                    if maskStr.[i] = '0' then
                        randomChar ()
                    else
                        '1')
        )

    let randomizeGrille (mask: string) =
        mask.Split($"{Environment.NewLine}")
        |> Seq.fold
            (fun (sb: Text.StringBuilder) s -> sb.Append($"{randomizeString (s)}{Environment.NewLine}"))
            (Text.StringBuilder())
        |> fun x -> x.ToString()

    let fillGrille (text: string) (grille: string) =
        let mutable newGrille = grille

        for c in text.ToCharArray() do
            let charArray = newGrille.ToCharArray()
            charArray.[newGrille.IndexOf('1')] <- c
            newGrille <- String(charArray)

        newGrille
        
    let findAllIndexes (mask: string) (char: char) =
        seq{
            for i in 0..(mask.Length-1) do
                if (mask.[i] = char) then
                    i
        }
        
    let decodeGrille (grille: string) (mask: string) =
        let indexes = findAllIndexes mask '1'
        if Seq.length indexes < 2 then
            None
        else
            let mutable resultString = ""
            for i in indexes do
                resultString <- resultString + string grille.[i]
            Some resultString
