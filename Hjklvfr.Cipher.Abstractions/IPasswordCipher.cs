﻿namespace Hjklvfr.Cipher.Abstractions
{
    public interface IPasswordCipher
    {
        string Encrypt(string text, string password);
        
        string Decrypt(string text, string password);
    }
}