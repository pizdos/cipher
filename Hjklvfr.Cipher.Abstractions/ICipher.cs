﻿namespace Hjklvfr.Cipher.Abstractions
{
    public interface ICipher
    {
        string Encrypt(string text);
        
        string Decrypt(string text);
    }
}