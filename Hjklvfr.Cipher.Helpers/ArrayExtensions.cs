﻿using System;

namespace Hjklvfr.Cipher.Helpers
{
    public static class ArrayExtensions
    {
        public static bool FindSymbol<T>(this T[,] symbolsTable, T symbol, out int column, out int row)
            where T : IComparable, IEquatable<T>
        {
            var l = symbolsTable.GetUpperBound(0) + 1;
            for (var i = 0; i < l; i++)
            {
                for (var j = 0; j < l; j++)
                {
                    if (!symbolsTable[i, j].Equals(symbol)) continue;
                    row = i;
                    column = j;
                    return true;
                }
            }

            row = -1;
            column = -1;
            return false;
        }
    }
}