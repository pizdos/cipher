﻿open System
open CardanGrille

[<EntryPoint>]
let main argv =
    printfn $"1.Encode{Environment.NewLine}2.Decode"
    let inputChoice = Console.ReadLine()
    match inputChoice with
    | "1" ->
        printf "Enter string: "
        let inputString = System.Console.ReadLine()
        let mask = CardanGrille.generateMask inputString.Length
        printfn $"{mask}"
        let randomizeGrille = CardanGrille.randomizeGrille mask
        printfn $"{CardanGrille.fillGrille (inputString.ToUpper()) randomizeGrille}"
    | "2" ->
        printf "Enter grille: "
        let inputGrille = Console.ReadLine()
        let grille = inputGrille.Replace("\n", "").Replace(" " , "")
        let sqrt = Math.Sqrt(float grille.Length)
        if (sqrt <> Math.Floor(sqrt)) then
            failwith "grille is not square"
        printf "Enter mask: "
        let inputMask = Console.ReadLine()
        let mask = inputMask.Replace("\n", "").Replace(" " , "")
        if (mask.Length <> grille.Length) then
            failwith "mask is wrong"
        
        printfn "Decode:"
        let decoded = CardanGrille.decodeGrille grille mask
        match decoded with
        | None -> failwith "501"
        | Some(s) ->
            if (s.Length <> 0) then
                printfn $"{s}"
            else
                printfn "Nothing"
    | _ -> failwith "1 or 2"
    
    //FPLHLXUEUWKQQYLFPEULJRCGO
    //0001000100000010000100001
    0