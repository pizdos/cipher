using System;
using Hjklvfr.Cipher.Abstractions;

namespace Hjklvfr.Cipher.App
{
    public class App
    {
        private readonly IPasswordCipher _passwordCipher;

        public App(IPasswordCipher passwordCipher)
        {
            _passwordCipher = passwordCipher;
        }

        public void Run(string[] args)
        {
            Console.WriteLine("Select action");
            Console.WriteLine("1. Encrypt");
            Console.WriteLine("2. Decrypt");
            var userSelected = Console.ReadLine();

            if (userSelected == "1")
            {
                Console.WriteLine("Enter text");
                var text = Console.ReadLine();
            
                Console.WriteLine("Enter password");
                var password = Console.ReadLine();

                var encrypted = _passwordCipher.Encrypt(text, password);
                Console.WriteLine("Your encrypted text");
                Console.WriteLine(encrypted);
            }
            else
            {
                Console.WriteLine("Enter encrypted text");
                var text = Console.ReadLine();
            
                Console.WriteLine("Enter password");
                var password = Console.ReadLine();

                var decrypted = _passwordCipher.Decrypt(text, password);
                Console.WriteLine(decrypted);
            }
        }
    }
}