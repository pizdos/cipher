﻿using System;
using Hjklvfr.Cipher.PolybiusSquare.Extensions;
using Microsoft.Extensions.DependencyInjection;

namespace Hjklvfr.Cipher.App
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var services = new ServiceCollection();
            ConfigureServices(services);

            var serviceProvider = services.BuildServiceProvider();

            // entry to run app
            var app = serviceProvider.GetService<App>() ?? throw new Exception("App not found");
            app.Run(args);
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<App>();
            services.AddPolybiusSquareCipher();
        }
    }
}